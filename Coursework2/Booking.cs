﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to define a Booking. A Booking can be associated to only one customer.  
///<summary>
///Date last modified: 09/12/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Concrete booking

namespace Coursework2
{
    [Serializable]
    public class Booking
    {
        private DateTime arrivalDate, departureDate, carHireFrom, carHireTo;
        private string dietaryReqs, carHireDrName;
        private int bookingRefNum;
        private static int bookingCounter;
        private bool breakfast, meals, carHire;

        private List<Guest> guestList = new List<Guest>(4);      

        //Every time the constructor is invoked, increase the booking counter by 1 - thread safe
        public Booking ()
        {
            bookingRefNum = System.Threading.Interlocked.Increment(ref bookingCounter);
        }
        
        //Getter and setters for safe access of private properties
        public DateTime ArrivalDate
        {
            get { return arrivalDate; }
            set { arrivalDate = value; }
        }
        public DateTime DepartureDate
        {
            get { return departureDate; }
            set { departureDate = value; }
        }
        public DateTime CarHireFrom
        {
            get { return carHireFrom; }
            set { carHireFrom = value; }
        }
        public DateTime CarHireTo
        {
            get { return carHireTo; }
            set { carHireTo = value; }
        }
        public string DietaryReqs
        {
            get { return dietaryReqs; }
            set { dietaryReqs = value; }
        }
        public string CarHireDrName
        {
            get { return carHireDrName; }
            set { carHireDrName = value; }
        }
        public static int BookingCounter
        {
            get { return bookingCounter; }
            set { bookingCounter = value; }
        }
        public bool Breakfast
        {
            get { return breakfast; }
            set { breakfast = value; }
        }
        public bool Meals
        {
            get { return meals; }
            set { meals = value; }
        }
        public bool CarHire
        {
            get { return carHire; }
            set { carHire = value; }
        }
        public List<Guest> GuestList
        {
            get { return guestList; }
            set { guestList = value; }
        }

        //Methods for invoicing - methods are implemented here. Reason being, should prices change, we will only have
        //to modify the booking class, with no modification to the caller class
        public double GetCostPerNight()
        {
            double costPerNight = 0;

            foreach (Guest guest in GuestList)
            {
                if (guest.Age < 18)
                    costPerNight += 30;
                else
                    costPerNight += 50;
            }
            return costPerNight;
        }

        public int GetStayingLength()
        {
            return DepartureDate.Subtract(ArrivalDate).Days;
        }

        public double GetBreakfastTotPrice(int nights)
        {
            if (Breakfast)
                return 5 * GuestList.Count * nights;
            return 0;
        }

        public double GetMealsTotPrice(int nights)
        {
            if (Meals)
                return 15 * GuestList.Count * nights;
            return 0;
        }

        public double GetCarHireTotPrice()
        {
            if (CarHire)
                return 50 * CarHireTo.Subtract(CarHireFrom).Days;
            return 0;
        }


        //Method that define the representation of a booking - used by comboboxes in the main window
        public override string ToString()
        {
            return "bookRefN: " + bookingRefNum + ", Arrival: " + arrivalDate.ToShortDateString() + 
                ", Departure: " + departureDate.ToShortDateString();
        }
    }
}

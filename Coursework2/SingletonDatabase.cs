﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///SingletonDatabase class. Here data is saved and loaded through serialisation.
///Class has been defined as a Singleton so to prevent it to be instanciated more than once.
///<summary>
///Date last modified: 05/12/2016

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2
{
    public sealed class SingletonDatabase
    {
        const string FileName = @"TestFile.bin";
        private static readonly SingletonDatabase instance = new SingletonDatabase();

        //Private contructor
        private SingletonDatabase() { }

        public static SingletonDatabase Instance
        {
            get { return instance; }
        }

        //Serialize to save
        public void FileSave(List<Customer> custList)
        {
            Stream FileStream = File.Create(FileName);
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(FileStream, custList);
            serializer.Serialize(FileStream, Customer.CustCounter);
            serializer.Serialize(FileStream, Booking.BookingCounter);
            FileStream.Close();
        }


        //Load saved file
        public List<Customer> FileLoad()
        {
            //When program is run, re-create saved customer list
            List<Customer> custList = new List<Customer>();

            if (File.Exists(FileName))
            {
                Stream FileStream = File.OpenRead(FileName);
                BinaryFormatter deserializer = new BinaryFormatter();
                custList = (List<Customer>)deserializer.Deserialize(FileStream);
                Customer.CustCounter = (int)deserializer.Deserialize(FileStream);
                Booking.BookingCounter = (int)deserializer.Deserialize(FileStream);
                FileStream.Close();
            }
            return custList;
        }
    }
}

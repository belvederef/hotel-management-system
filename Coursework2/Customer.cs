﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to define a Customer type of Person.
///Customers can have several bookings.
///<summary>
///Date last modified: 08/12/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2
{
    [Serializable]
    public class Customer : Person
    {
        private string address;
        private int custRefNum;
        private static int custCounter;
        private List<Booking> bookingList = new List<Booking>();

        //Every time the constructor is invoked, increase custCounter by 1 - thread safe
        public Customer (string name, string address) : base(name)
        {
            this.address = address;
            custRefNum = System.Threading.Interlocked.Increment(ref custCounter);  
        }

        //Getter and setters for safe access of private properties
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public static int CustCounter
        {
            get { return custCounter; }
            set { custCounter = value; }
        }

        public List<Booking> BookingList
        {
            get { return bookingList; }
            set { bookingList = value; }
        }

        //Method that define the representation of a customer - used by comboboxes in the main window
        public override string ToString()
        {
            return custRefNum + " || " + Name + " || " + address;
        }
    }
}

﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///Fluent Interface design pattern. The class is not using all of the Fluent Interface 
///features. It has the great advantage to make the syntax used from the caller class
///much simpler and easier to understand from any type of developer.
///It is used to add a new booking or amend an existing booking dependin on the first method used.
///<summary>
///Date last modified: 30/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2
{
    class BookingFluent
    {
        private Booking booking;

        public BookingFluent CreateBooking()
        {
            booking =  new Booking();
            return this;
        }

        public BookingFluent Amend(Booking bookingToAmend)
        {
            booking = bookingToAmend;
            return this;
        }

        public BookingFluent ArrivalDate(DateTime? date)
        {
            if(date != null)
                booking.ArrivalDate = date.Value;
            return this;
        }

        public BookingFluent DepartureDate(DateTime? date)
        {
            if (date != null)
                booking.DepartureDate = date.Value;
            return this;
        }

        public BookingFluent Breakfast(bool boolValue)
        {
            booking.Breakfast = boolValue;
            return this;
        }

        public BookingFluent Meals(bool boolValue)
        {
            booking.Meals = boolValue;
            return this;
        }

        public BookingFluent DietaryReqs(string s)
        {
            if(booking.Breakfast || booking.Meals)
                booking.DietaryReqs = s;
            return this;
        }

        public BookingFluent CarHire(bool boolValue)
        {
            booking.CarHire = boolValue;
            return this;
        }

        public BookingFluent CarHireFrom(DateTime? date)
        {
            if(booking.CarHire && date != null)
                booking.CarHireFrom = date.Value;
            return this;
        }

        public BookingFluent CarHireTo(DateTime? date)
        {
            if (booking.CarHire && date != null)
                booking.CarHireTo = date.Value;
            return this;
        }

        public BookingFluent CarHireDrName(string s)
        {
            if (booking.CarHire)
                booking.CarHireDrName = s;
            return this;
        }

        public Booking Value()
        {
            return booking;
        }
    }
}

﻿#pragma checksum "..\..\Window1.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "996E37F2DB0641E455A41AD2B1AA583F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Coursework2;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Coursework2 {
    
    
    /// <summary>
    /// Window1
    /// </summary>
    public partial class Window1 : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gboxSelReserv;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxSelReserv;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAmendBooking;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDeleteBooking;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gboxCustDetails;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCustName;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCustAddress;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxCustName;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxCustAddress;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gboxGuestDetails;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGuestNum;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGuestName;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGuestPassN;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGuestAge;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxGuestName;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxGuestPassN;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxGuestN;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxGuestAge;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuestDelete;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuestAdd;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gboxBooking;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDateFrom;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDateTo;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxBookingFrom;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gboxExtra;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblEvenMelas_Copy;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkboxBreakf;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkboxMeals;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxBookingTo;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxDietaryReqs;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkboxCarHire;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCarHireFrom;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCarHireTo;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxCarHireFrom;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxCarHireTo;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCarHireDrName;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tboxCarHireDrName;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBill;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAddBooking;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxSelReservCust;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSelCustName;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSelBook;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCustAdd;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCustDelete;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDone;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCustAmend;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\Window1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuestAmend;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Coursework2;component/window1.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Window1.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.gboxSelReserv = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 2:
            this.cboxSelReserv = ((System.Windows.Controls.ComboBox)(target));
            
            #line 14 "..\..\Window1.xaml"
            this.cboxSelReserv.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cboxSelReserv_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnAmendBooking = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.btnDeleteBooking = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.gboxCustDetails = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 6:
            this.lblCustName = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.lblCustAddress = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.tboxCustName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.tboxCustAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.gboxGuestDetails = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 11:
            this.lblGuestNum = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.lblGuestName = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.lblGuestPassN = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.lblGuestAge = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.tboxGuestName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.tboxGuestPassN = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.cboxGuestN = ((System.Windows.Controls.ComboBox)(target));
            
            #line 29 "..\..\Window1.xaml"
            this.cboxGuestN.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cboxGuestN_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 18:
            this.tboxGuestAge = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.btnGuestDelete = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\Window1.xaml"
            this.btnGuestDelete.Click += new System.Windows.RoutedEventHandler(this.btnGuestDelete_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.btnGuestAdd = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\Window1.xaml"
            this.btnGuestAdd.Click += new System.Windows.RoutedEventHandler(this.btnGuestAdd_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.gboxBooking = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 22:
            this.lblDateFrom = ((System.Windows.Controls.Label)(target));
            return;
            case 23:
            this.lblDateTo = ((System.Windows.Controls.Label)(target));
            return;
            case 24:
            this.tboxBookingFrom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.gboxExtra = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 26:
            this.lblEvenMelas_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 27:
            this.chkboxBreakf = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 28:
            this.chkboxMeals = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 29:
            this.tboxBookingTo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.tboxDietaryReqs = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.chkboxCarHire = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 32:
            this.lblCarHireFrom = ((System.Windows.Controls.Label)(target));
            return;
            case 33:
            this.lblCarHireTo = ((System.Windows.Controls.Label)(target));
            return;
            case 34:
            this.tboxCarHireFrom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 35:
            this.tboxCarHireTo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 36:
            this.lblCarHireDrName = ((System.Windows.Controls.Label)(target));
            return;
            case 37:
            this.tboxCarHireDrName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 38:
            this.btnBill = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\Window1.xaml"
            this.btnBill.Click += new System.Windows.RoutedEventHandler(this.btnBill_Click);
            
            #line default
            #line hidden
            return;
            case 39:
            this.btnAddBooking = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\Window1.xaml"
            this.btnAddBooking.Click += new System.Windows.RoutedEventHandler(this.btnAddBooking_Click);
            
            #line default
            #line hidden
            return;
            case 40:
            this.cboxSelReservCust = ((System.Windows.Controls.ComboBox)(target));
            
            #line 52 "..\..\Window1.xaml"
            this.cboxSelReservCust.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cboxSelReservCust_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 41:
            this.lblSelCustName = ((System.Windows.Controls.Label)(target));
            return;
            case 42:
            this.lblSelBook = ((System.Windows.Controls.Label)(target));
            return;
            case 43:
            this.btnCustAdd = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\Window1.xaml"
            this.btnCustAdd.Click += new System.Windows.RoutedEventHandler(this.btnCustAdd_Click);
            
            #line default
            #line hidden
            return;
            case 44:
            this.btnCustDelete = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\Window1.xaml"
            this.btnCustDelete.Click += new System.Windows.RoutedEventHandler(this.btnCustDelete_Click);
            
            #line default
            #line hidden
            return;
            case 45:
            this.btnDone = ((System.Windows.Controls.Button)(target));
            
            #line 57 "..\..\Window1.xaml"
            this.btnDone.Click += new System.Windows.RoutedEventHandler(this.btnAddGuests_Click);
            
            #line default
            #line hidden
            return;
            case 46:
            this.btnCustAmend = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\Window1.xaml"
            this.btnCustAmend.Click += new System.Windows.RoutedEventHandler(this.btnCustAmend_Click);
            
            #line default
            #line hidden
            return;
            case 47:
            this.btnGuestAmend = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\Window1.xaml"
            this.btnGuestAmend.Click += new System.Windows.RoutedEventHandler(this.btnGuestDelete_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}


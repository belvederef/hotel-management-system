﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to define a Guest type of Person. A Customer is not necessarily a Guest. 
///<summary>
///Date last modified: 28/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2
{
    [Serializable]
    public class Guest : Person
    {
        private string passportNum;
        private int age;

        //Constructors
        public Guest (string name, string passportNum, int age) : base(name)
        {
            this.passportNum = passportNum;
            this.age = age;
        }
 
        public string PassportNum 
        { 
            get { return passportNum; }
            set { passportNum = value; }
        }
        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public override string ToString()
        {
            return Name + " || " + passportNum + " || " + age;                 
        }
    }
}

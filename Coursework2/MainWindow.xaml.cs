﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///GUI - main Window class.
///<summary>
///Date last modified: 05/12/2016

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Coursework2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Customer> custList;

        public MainWindow()
        {
            InitializeComponent();
            custList = SingletonDatabase.Instance.FileLoad();
            loadForm();
        }


        //Method to add a customer to the customer list
        private void btnCustAdd_Click(object sender, RoutedEventArgs e)
        {
            bool custExists = this.custExists();

            //Check if customer is present in the list. If not present, add them
            if (custExists == true)
            {
                MessageBox.Show("Customer already present in the system!");
                return;
            }

            Customer cust = new Customer(tboxCustName.Text, tboxCustAddress.Text);
            custList.Insert(cboxSelCust.Items.Count, cust);
            cboxSelCust.Items.Add(cust);
            cboxSelCust.SelectedIndex = custList.Count - 1;
        }


        //Method to delete a customer
        private void btnCustDelete_Click(object sender, RoutedEventArgs e)
        {
            //Delete a customer. Check whether the selected customer has any bookings. If so, suggest to delete them first
            if (cboxSelCust.SelectedIndex == -1)
            {
                MessageBox.Show("Select customer from list first!");
            }
            else if (custList[cboxSelCust.SelectedIndex].BookingList.Count == 0)
            {
                custList.RemoveAt(cboxSelCust.SelectedIndex);
                cboxSelCust.Items.RemoveAt(cboxSelCust.SelectedIndex);
                tboxCustName.Clear();
                tboxCustAddress.Clear();
            }
            else
            {
                MessageBox.Show("You cannot delete a customer that has bookings! Delete bookings first!");
            }
        }


        //Method to modify a customer's details
        private void btnCustAmend_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelCust.SelectedIndex == -1)
            {
                MessageBox.Show("Select a customer first!");
                return;
            }

            custList[cboxSelCust.SelectedIndex].Name = tboxCustName.Text;
            custList[cboxSelCust.SelectedIndex].Address = tboxCustAddress.Text;

            //Update combobox value
            updateCustCombobox();
        }


        //Method to add a booking to a customer's booking list
        private void btnAddBooking_Click(object sender, RoutedEventArgs e)
        {
            //If no custumer is selected, prompt to choose one
            if (cboxSelCust.SelectedIndex == -1)
            {
                MessageBox.Show("Select a customer first!");
                return;
            }

            if (checkDates() == false)
                return;

            BookingFluent booking = new BookingFluent();

            booking.CreateBooking()
                         .ArrivalDate(dpickArrivalDate.SelectedDate)
                         .DepartureDate(dpickDepartureDate.SelectedDate)
                         .Breakfast(chkboxBreakf.IsChecked.Value)
                         .Meals(chkboxMeals.IsChecked.Value)
                         .DietaryReqs(tboxDietaryReqs.Text)
                         .CarHire(chkboxCarHire.IsChecked.Value)
                         .CarHireFrom(dpickCarHireFrom.SelectedDate)
                         .CarHireTo(dpickCarHireTo.SelectedDate)
                         .CarHireDrName(tboxCarHireDrName.Text);


            //Insert value into customer's booking list and update combobox
            custList[cboxSelCust.SelectedIndex].BookingList.Insert(cboxSelReserv.Items.Count, booking.Value());
            cboxSelReserv.Items.Add(booking.Value());
            cboxSelReserv.SelectedIndex = custList[cboxSelCust.SelectedIndex].BookingList.Count - 1;
        }


        //Method to delete a booking
        private void btnDeleteBooking_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelReserv.SelectedIndex == -1)
            {
                MessageBox.Show("Select a booking from list first!");
                return;
            }

            custList[cboxSelCust.SelectedIndex].BookingList.RemoveAt(cboxSelReserv.SelectedIndex);
            cboxSelReserv.Items.RemoveAt(cboxSelReserv.SelectedIndex);
            ClearBooking();
        }


        //Method to modify a booking
        private void btnAmendBooking_Click(object sender, RoutedEventArgs e)
        {
            if (cboxSelReserv.SelectedIndex == -1)
            {
                MessageBox.Show("Select a booking from list first!");
                return;
            }

            if (checkDates() == false)
                return;

            BookingFluent booking = new BookingFluent();

            //To keep the code tidy
            var thisBooking = custList[cboxSelCust.SelectedIndex].BookingList[cboxSelReserv.SelectedIndex];

            booking.Amend(thisBooking)
                         .ArrivalDate(dpickArrivalDate.SelectedDate)
                         .DepartureDate(dpickDepartureDate.SelectedDate)
                         .Breakfast(chkboxBreakf.IsChecked.Value)
                         .Meals(chkboxMeals.IsChecked.Value)
                         .DietaryReqs(tboxDietaryReqs.Text)
                         .CarHire(chkboxCarHire.IsChecked.Value)
                         .CarHireFrom(dpickCarHireFrom.SelectedDate)
                         .CarHireTo(dpickCarHireTo.SelectedDate)
                         .CarHireDrName(tboxCarHireDrName.Text);

            //Booking that needs to be amended is equal to the temporary one modified
            thisBooking = booking.Value();

            //Update combobox value
            updateBookingCombobox();
        }


        //Method to add a guest to a booking's guestlist
        private void btnGuestAdd_Click(object sender, RoutedEventArgs e)
        {
            //If no booking is selected, prompt to choose one
            if (cboxSelCust.SelectedIndex == -1)
            {
                MessageBox.Show("Select a booking first!");
                return;
            }

            if (checkGuestSection() == false)
                return;


            //To keep the code tidy
            var thisGuestList = custList[cboxSelCust.SelectedIndex].BookingList[cboxSelReserv.SelectedIndex].GuestList;

            if (thisGuestList.Count < 4)              
            {
                Guest guest = new Guest(tboxGuestName.Text, tboxGuestPassN.Text, Convert.ToInt32(tboxGuestAge.Text));
                thisGuestList.Insert(cboxGuest.Items.Count, guest);

                //Update combobox
                cboxGuest.Items.Add(guest);
                ClearGuestCombobox();
            }
            else
            {
                MessageBox.Show("You cannot add more than 4 guests!");
            }
        }


        //Method to delete a guest from a booking's guestlist
        private void btnGuestDelete_Click(object sender, RoutedEventArgs e)
        {
            if (cboxGuest.SelectedIndex == -1)
            {
                MessageBox.Show("Select guest from list first!");
                return;
            }

            //To keep the code tidy
            var thisGuestList = custList[cboxSelCust.SelectedIndex].BookingList[cboxSelReserv.SelectedIndex].GuestList;
            thisGuestList.RemoveAt(cboxGuest.SelectedIndex);

            //Update combobox
            cboxGuest.Items.RemoveAt(cboxGuest.SelectedIndex);
            ClearGuestCombobox();
        }


        //Method to modify a guest
        private void btnGuestAmend_Click(object sender, RoutedEventArgs e)
        {
            //If no guest is selected, prompt to choose one
            if (cboxGuest.SelectedIndex == -1)
            {
                MessageBox.Show("Select a guest from list first!");
                return;
            }

            if (checkGuestSection() == false)
                return;


            //To keep the code tidy
            var thisGuestList = custList[cboxSelCust.SelectedIndex].BookingList[cboxSelReserv.SelectedIndex].GuestList;

            thisGuestList[cboxGuest.SelectedIndex].Name = tboxGuestName.Text;
            thisGuestList[cboxGuest.SelectedIndex].PassportNum = tboxGuestPassN.Text;
            thisGuestList[cboxGuest.SelectedIndex].Age = Convert.ToInt32(tboxGuestAge.Text);

            //Update combobox
            int selIndx;
            selIndx = cboxGuest.SelectedIndex;
            cboxGuest.Items.RemoveAt(cboxGuest.SelectedIndex);
            cboxGuest.Items.Insert(selIndx, thisGuestList[selIndx]);
            cboxGuest.SelectedIndex = selIndx;
        }


        //Method that defines what happens when a value in the customer combobox is chosen
        private void cboxSelCust_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboxSelCust.SelectedIndex == -1)
                return;

            ClearBooking();
            cboxSelReserv.Items.Clear();
            tboxCustName.Text = custList[cboxSelCust.SelectedIndex].Name;
            tboxCustAddress.Text = custList[cboxSelCust.SelectedIndex].Address;

            //For every customer's booking, add an option to retrieve it from the appropriate combobox
            foreach (Booking booking in custList[cboxSelCust.SelectedIndex].BookingList)
            {
                cboxSelReserv.Items.Add(booking);
            }
        }


        //Method that defines what happens when a booking is chosen 
        private void cboxSelReserv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboxSelReserv.SelectedIndex == -1)
                return;

            //To keep the code tidy
            var thisBooking = custList[cboxSelCust.SelectedIndex].BookingList[cboxSelReserv.SelectedIndex];
            ClearBooking();

            //Update booking fields
            dpickArrivalDate.SelectedDate = thisBooking.ArrivalDate;
            dpickDepartureDate.SelectedDate = thisBooking.DepartureDate;
            chkboxBreakf.IsChecked = thisBooking.Breakfast;
            chkboxMeals.IsChecked = thisBooking.Meals;
            chkboxCarHire.IsChecked = thisBooking.CarHire;
            tboxDietaryReqs.Text = thisBooking.DietaryReqs;

            if (chkboxCarHire.IsChecked.Value)
            {
                dpickCarHireFrom.SelectedDate = thisBooking.CarHireFrom;
                dpickCarHireTo.SelectedDate = thisBooking.CarHireTo;
                tboxCarHireDrName.Text = thisBooking.CarHireDrName;
            }

            //For every booking, load into the guestlist combobox all the related guests
            foreach (Guest guest in thisBooking.GuestList)
            {
                cboxGuest.Items.Add(guest);
            }
        }


        //Method that defines what happens when a guest is chosen
        private void cboxGuest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboxGuest.SelectedIndex == -1)
                return;

            //To keep the code tidy
            var thisGuest = custList[cboxSelCust.SelectedIndex].BookingList[cboxSelReserv.SelectedIndex].GuestList[cboxGuest.SelectedIndex];

            tboxGuestName.Text = thisGuest.Name;
            tboxGuestPassN.Text = thisGuest.PassportNum;
            tboxGuestAge.Text = thisGuest.Age.ToString();
        }


        //When the bill button is clicked, pass the selected booking to the invoice class and produce an invoice 
        private void btnBill_Click(object sender, RoutedEventArgs e)
        {
            //If no booking is selected, prompt to choose one
            if (cboxSelReserv.SelectedIndex == -1)
            {
                MessageBox.Show("Select a booking from list first!");
                return;
            }

            Invoice invoice = new Invoice(custList[cboxSelCust.SelectedIndex].BookingList[cboxSelReserv.SelectedIndex]);
            invoice.Show();
        }


        //When the form is closed, save data in local
        private void window_Closed(object sender, EventArgs e)
        {
            SingletonDatabase.Instance.FileSave(custList);
        }


        private bool custExists()
        {
            //Check whether the customer is already in the system. As soon as it finds it, return true
            foreach (Customer customer in custList)
            {
                if (customer.Name == tboxCustName.Text && customer.Address == tboxCustAddress.Text)
                    return true;
            }
            return false;
        }




        //UTILS

        private void updateCustCombobox()
        {
            int selIndx;
            selIndx = cboxSelCust.SelectedIndex;
            cboxSelCust.Items.RemoveAt(cboxSelCust.SelectedIndex);
            cboxSelCust.Items.Insert(selIndx, custList[selIndx]);
            cboxSelCust.SelectedIndex = selIndx;
        }


        private void updateBookingCombobox()
        {
            int selIndx;
            selIndx = cboxSelReserv.SelectedIndex;
            cboxSelReserv.Items.RemoveAt(cboxSelReserv.SelectedIndex);
            cboxSelReserv.Items.Insert(selIndx, custList[cboxSelCust.SelectedIndex].BookingList[selIndx]);
            cboxSelReserv.SelectedIndex = selIndx;
        }


        private void loadForm()
        {
            //Re-insert data into the customer list combobox
            foreach (Customer cust in custList)
            {
                cboxSelCust.Items.Add(cust);
            }
        }


        //Check whether the booking object is in a good format before saving it
        private bool checkDates()
        {
            if (dpickArrivalDate.SelectedDate == null)
            {
                MessageBox.Show("Select an arrival date!");
                return false;
            }

            if (dpickDepartureDate.SelectedDate == null)
            {
                MessageBox.Show("Select a departure date!");
                return false;
            }

            if (chkboxCarHire.IsChecked.Value)
            {
                if (dpickCarHireFrom.SelectedDate == null)
                {
                    MessageBox.Show("Car Hire extra is selected! Choose a starting date!");
                    return false;
                }

                if (dpickCarHireTo.SelectedDate == null)
                {
                    MessageBox.Show("Car Hire extra is selected! Choose an ending date!");
                    return false;
                }
            }
            return true;
        }


        //Check whether the guest section has been filled correctly
        private bool checkGuestSection()
        {
            if (tboxGuestPassN.Text.Length > 10)
            {
                MessageBox.Show("Passport number cannot be longer than 10 digits");
                return false;
            }

            int result;
            if (!int.TryParse(tboxGuestAge.Text, out result)  || result < 0 || result > 101)
            {
                MessageBox.Show("Guest's age must be a number between 0 and 101");
                return false;
            }
            return true;
        }





        ///SOME METHODS TO TIDY UP

        //Clear the subsequent fields when the selected customer changes
        public void ClearBooking()
        {
            dpickArrivalDate.SelectedDate = null;
            dpickDepartureDate.SelectedDate = null;
            chkboxBreakf.IsChecked = false;
            chkboxMeals.IsChecked = false;
            tboxDietaryReqs.Clear();
            chkboxCarHire.IsChecked = false;
            dpickCarHireFrom.SelectedDate = null;
            dpickCarHireTo.SelectedDate = null;
            tboxCarHireDrName.Clear();
            cboxGuest.Items.Clear();
            ClearGuestCombobox();
        }


        //Clear the form when a guest is added, amended or deleted
        public void ClearGuestCombobox()
        {
            cboxGuest.SelectedIndex = -1;
            tboxGuestName.Clear();
            tboxGuestPassN.Clear();
            tboxGuestAge.Clear();
        }
    }
}




﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to define an invoice window. It is connected to the GUI class.
///<summary>
///Date last modified: 22/11/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework2
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        private int nights;

        public Invoice()
        {
            InitializeComponent();
        }

        internal Invoice(Booking booking)
        {
            InitializeComponent();

            nights = booking.GetStayingLength();

            //Calcultate bill and update invoice window content
            lblCostPerNight2.Content = booking.GetCostPerNight();
            lblBreakfastCost2.Content = booking.GetBreakfastTotPrice(nights);
            lblMealsCost2.Content = booking.GetMealsTotPrice(nights);
            lblCarHireCost2.Content = booking.GetCarHireTotPrice();
            lblTotCost2.Content = ((double)lblCostPerNight2.Content * nights) + (double)lblBreakfastCost2.Content 
                + (double)lblMealsCost2.Content + (double)lblCarHireCost2.Content;
        }
    }
}

﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///Abstract class Person. Customers and Guests inherit from this class. 
///<summary>
///Date last modified: 09/12/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework2
{
    [Serializable]
    public abstract class Person
    {
        private string name;

        protected Person (string name)
        {
            this.name = name;
        }
 
        public string Name
        {
            get{ return name; }
            set{ name = value; }
        }
    }
}

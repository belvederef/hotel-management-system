﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///Unit Test. This Unit tests all the booking class methods, so to compare the expected results (simply calculated 
///the old way) to the ones obtained from the aformentioned methods.
///<summary>
///Date last modified: 06/12/2016

using System;
using System.Collections.Generic;
using Coursework2;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetCostPerNight_WithValidAmount()
        {
            //arrange
            List<Guest> guestList = new List<Guest>();
            guestList.Add(new Guest("guest1", "guest1", 16));
            guestList.Add(new Guest("guest2", "guest2", 30));
            double expectedCostPerNight = 80;

            Booking booking = new Booking();
            booking.GuestList = guestList;

            //act
            double actualCostPerNight = booking.GetCostPerNight();

            //assert
            Assert.AreEqual(expectedCostPerNight, actualCostPerNight,
                "Cost per night not calculated properly");
        }


        [TestMethod]
        public void GetStayingLength_WithValidDates()
        {
            //arrange
            DateTime arrivalDate = Convert.ToDateTime("10/11/2016");
            DateTime departureDate = Convert.ToDateTime("15/11/2016");
            int expectedLength = 5;

            Booking booking = new Booking();
            booking.ArrivalDate = arrivalDate;
            booking.DepartureDate = departureDate;

            // act
            int actualLength = booking.GetStayingLength();

            // assert
            Assert.AreEqual(expectedLength, actualLength, "Staying length not calculated correctly");
        }


        [TestMethod]
        public void GetBreakfastTotPrice_WithValidValues()
        {
            //arrange
            List<Guest> guestList = new List<Guest>();
            guestList.Add(new Guest("guest1", "guest1", 16));
            guestList.Add(new Guest("guest2", "guest2", 30));

            double expectedBreakfastPrice = 50;     //expected result for five nights for two guests

            Booking booking = new Booking();
            booking.GuestList = guestList;
            booking.Breakfast = true;

            //act
            double actualBreakfastPrice = booking.GetBreakfastTotPrice(5);  //5 is the number of nights

            //assert
            Assert.AreEqual(expectedBreakfastPrice, actualBreakfastPrice,
                "Breakfast total price not calculated correctly");
        }


        [TestMethod]
        public void GetMealsTotPrice_WithValidValues()
        {
            //arrange
            List<Guest> guestList = new List<Guest>();
            guestList.Add(new Guest("guest1", "guest1", 16));
            guestList.Add(new Guest("guest2", "guest2", 30));

            double expectedMealsPrice = 120;     //expected result for four nights for two guests

            Booking booking = new Booking();
            booking.GuestList = guestList;
            booking.Meals = true;

            //act
            double actualMealsPrice = booking.GetMealsTotPrice(4);      //4 is the number of nights

            //assert
            Assert.AreEqual(expectedMealsPrice, actualMealsPrice, 
                "Meals total price not calculated correctly");
        }


        [TestMethod]
        public void GetCarHireTotPrice_WithValidValues()
        {
            //arrange
            DateTime startDate = Convert.ToDateTime("10/11/2016");
            DateTime endDate = Convert.ToDateTime("15/11/2016");
            double expectedCarHireCost = 250;

            Booking booking = new Booking();
            booking.CarHire = true;
            booking.CarHireFrom = startDate;
            booking.CarHireTo = endDate;

            //act
            double actualCarHireCost = booking.GetCarHireTotPrice();

            //assert
            Assert.AreEqual(expectedCarHireCost, actualCarHireCost,
                "Car Hire total cost not calculated correctly");
        }
    }
}

WPF application, c# developed.
The system allows bookings to be added, amended and deleted.
The system is be able to cope with any number of customers, each of whom may have multiple bookings. It is be possible to amend a booking (including adding or removing extras) once it has been entered. The system is also be able to produce invoices showing the cost of a booking, the invoice shows the
costs per night and the costs of any extras.

A unit test has been included.
Data is stored through serialisation.

University project.